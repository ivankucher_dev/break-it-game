package com.epam.trainings.levelstrategy;

import com.epam.trainings.model.Model;
/**
 * Context class provide a way for the user to play different levels.
 *
 * @author epam_6th_team_lab24
 * @version 1.0
 * @since 2019-08-27
 */
public class LevelContext {

  private LevelStrategy strategy;
  /** Straregy pattern method to setting up concrete level */
  public void setLevelStrategy(LevelStrategy strategy) {
    this.strategy = strategy;
  }

  /** @see LevelStrategy for more information about this method */
  public void loadImagesForLevel(Model model) {
    strategy.setLevelImages(model);
  }

  /** @see LevelStrategy for more information about this method */
  public void setLevelBallSize(Model model) {
    strategy.setLevelBallSize(model);
  }

  /** @see LevelStrategy for more information about this method */
  public void setLevelBoardSize(Model model) {
    strategy.setLevelBoardSize(model);
  }

  /** @see LevelStrategy for more information about this method */
  public void setScoreBoardMessage(Model model) {
    strategy.setScoreBoardMessage(model);
  }

  /** @see LevelStrategy for more information about this method */
  public void initBlocks(Model model) {
    strategy.setLevelBLocks(model);
  }
}
