package com.epam.trainings.levelstrategy;

import com.epam.trainings.model.Model;
import static com.epam.trainings.utils.PanelWorker.setImagesForLevel;
/**
 * General interface for level strategy classes
 */
public interface LevelStrategy {
  /**
   * Method to set images for concrete level of game.
   * @param model parameter {@link Model} which contains all models .
   * Using for setuping images in models. Original method in
   * {@link com.epam.trainings.utils.PanelWorker}
   */
  void setLevelImages(Model model);
  /**
   * Method to setup ball parametrs for concrete level of game.
   * @param model parameter {@link Model} which contains all models .
   * Using for setuping ball size and also speed.
   */
  void setLevelBallSize(Model model);
  /**
   * Method to setup board parametrs for concrete level of game.
   * @param model parameter {@link Model} which contains all models .
   * Using for setuping board size and also speed.
   */
  void setLevelBoardSize(Model model);
  /**
   * Method to initialize game blocks for concrete level ( concrete width and height).
   * @param model parameter {@link Model} which contains all models .
   */
  void setLevelBLocks(Model model);
  /**
   * Method to set message for score board after user won or lost .
   * @param model parameter {@link Model} which contains all models .
   * Message depends on concrete level block and his 'game block image'.
   */
  void setScoreBoardMessage(Model model);
}
