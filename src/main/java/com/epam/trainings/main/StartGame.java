package com.epam.trainings.main;

import com.epam.trainings.controller.viewcontroller.gameovercontroller.GameOverController;
import com.epam.trainings.controller.viewcontroller.gameovercontroller.GameOverControllerImpl;
import com.epam.trainings.controller.viewcontroller.gameviewcontroller.GameViewController;
import com.epam.trainings.controller.viewcontroller.gameviewcontroller.GameViewControllerImpl;
import com.epam.trainings.controller.viewcontroller.settingsviewcontroller.SettingsViewController;
import com.epam.trainings.controller.viewcontroller.settingsviewcontroller.SettingsViewControllerImpl;
import com.epam.trainings.controller.viewcontroller.startmenucontroller.ViewController;
import com.epam.trainings.controller.viewcontroller.startmenucontroller.ViewControllerImpl;
import com.epam.trainings.model.Model;
import com.epam.trainings.view.GameOverPanel;
import com.epam.trainings.view.GamePanel;
import com.epam.trainings.view.MenuPanel;
import com.epam.trainings.view.SettingsPanel;
import com.epam.trainings.view.viewComponents.BackgroundPanel;
import com.epam.trainings.view.viewComponents.GameFrame;
/**
 * Initializer class. Start point of our program .
 * Here we initializing controllers , views and models
 * @author epam_6th_team_lab24
 */
public class StartGame {
  /**
   * Game view
   */
  private GamePanel gamePanel;
  /**
   * Menu view , load when program is started
   */
  private MenuPanel menuPanel;
  /**
   *Settings view on which user can setup his fps.
   */
  private SettingsPanel settingsPanel;
  private BackgroundPanel backgroundPanel;
  /**
   * Last view which shows user result score.
   */
  private GameOverPanel gameOverPanel;
  /**
   * Main frame of application.
   */
  private GameFrame gameFrame;
  /**
   * Main model , which contains all models of our app.
   */
  private Model model;
  /**
   * Controller of Menu view.
   */
  private ViewController controller;
  /**
   * Controller of main game process ( game view ).
   */
  private GameViewController gameController;
  /**
   * Controller of settings fps menu
   */
  private SettingsViewController settingsController;
  /**
   * Controller of game over view
   */
  private GameOverController gameOverController;

  StartGame() {
    initModels();
    initViews();
    initControllers();
  }
  /**
   * Method for initialize all models , need to be called first
   */
  private void initModels() {
    model = new Model();
  }

  /**
   * Method for initialize all views , need to be called second
   */
  private void initViews() {
    gameOverPanel = new GameOverPanel();
    gamePanel = new GamePanel(gameOverPanel);
    menuPanel = new MenuPanel(gamePanel);
    settingsPanel = new SettingsPanel(menuPanel);
    menuPanel.setSettingsPanel(settingsPanel);
    backgroundPanel = new BackgroundPanel();
    gameFrame = new GameFrame();
    gameOverPanel.setMenuPanel(menuPanel);
    gameFrame.init(menuPanel, backgroundPanel);
  }
  /**
   * Method for initialize all controllers , need to be called last
   */
  private void initControllers() {
    controller = new ViewControllerImpl(model, menuPanel);
    gameController = new GameViewControllerImpl(model, gamePanel);
    settingsController = new SettingsViewControllerImpl(model, settingsPanel);
    gameOverController = new GameOverControllerImpl(model, gameOverPanel);
  }
}
