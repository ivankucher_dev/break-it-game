package com.epam.trainings.utils;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
/**
 * class that gets file from URL
 *
 * @author epam_6th_team_lab24
 */
public class FileWorker {

  public static File getFileFromURL(String path, Object context) {
    URL url = context.getClass().getClassLoader().getResource(path);
    File file = null;
    try {
      file = new File(url.toURI());
    } catch (URISyntaxException e) {
      file = new File(url.getPath());
    } finally {
      return file;
    }
  }
}
