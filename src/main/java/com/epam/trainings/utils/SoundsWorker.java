package com.epam.trainings.utils;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class SoundsWorker {
  public static boolean muted = false;
  public static Clip soundNow;

  public static synchronized void playSound(String sound) {
    if (muted) {
      return;
    }
    new Thread(
            new Runnable() {
              public void run() {
                try {
                  Clip clip = AudioSystem.getClip();
                  AudioInputStream inputStream =
                      AudioSystem.getAudioInputStream(FileWorker.getFileFromURL(sound, this));
                  clip.open(inputStream);
                  clip.start();
                  soundNow = clip;
                } catch (Exception e) {
                  e.printStackTrace();
                }
              }
            })
        .start();
  }
}
