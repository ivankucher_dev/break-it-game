package com.epam.trainings.controller.viewcontroller.gameviewcontroller;

import javax.swing.*;
import java.awt.*;
/**
 * GameViewController Interface which provides main methods to work with GameOverPanel
 * {@link com.epam.trainings.view.GamePanel} view
 * @see com.epam.trainings.controller.viewcontroller.gameviewcontroller.GameViewControllerImpl
 * @author epam_6th_team_lab24
 * @version 1.0
 * @since 2019-08-27
 */
public interface GameViewController {
  /**
   * Method to providing opportunities for updating fps depends on game level
   * @param timer main Timer for updating view every couple millis
   * @param timeDelay frame rate
   */
  void updateFps(Timer timer, int timeDelay);
  /**
   * Method to draw ball , board and blocks on view
   * @param g graphics of panel
   */
  void draw(Graphics g);
}
