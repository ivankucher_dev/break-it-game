package com.epam.trainings.controller.viewcontroller.settingsviewcontroller;
/**
 * SettingsViewController Interface which provides main methods to work with SettingsView
 * {@link com.epam.trainings.view.SettingsPanel} view
 * @see com.epam.trainings.controller.viewcontroller.settingsviewcontroller.SettingsViewControllerImpl
 * @author epam_6th_team_lab24
 * @version 1.0
 * @since 2019-08-27
 */
public interface SettingsViewController  {
    /**
     * Method which takes fps as parameter from view
     * and depends on it changing fps of our game
     * @param fps get picked radio button from view
     */
    void changeFps(int fps);
}
