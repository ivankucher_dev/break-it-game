package com.epam.trainings.controller.viewcontroller.gameviewcontroller;

import com.epam.trainings.constants.UIConstant;
import com.epam.trainings.controller.ballcontroller.BallController;
import com.epam.trainings.controller.blockcontroller.BlockController;
import com.epam.trainings.controller.boardcontroller.BoardController;
import com.epam.trainings.model.Model;
import com.epam.trainings.model.modelClasess.Block;
import com.epam.trainings.view.GamePanel;
import javax.swing.*;
import java.awt.*;
import static com.epam.trainings.constants.Const.*;
import static com.epam.trainings.constants.UIConstant.*;
import static com.epam.trainings.utils.SoundsWorker.*;
/**
 * Controller of {@link com.epam.trainings.view.MenuPanel} view
 *
 * @author epam_6th_team_lab24
 * @version 1.0
 * @since 2019-08-27
 */
public class GameViewControllerImpl
    implements BallController, BlockController, BoardController, GameViewController {
  /** Field of model , need to access other models to update them */
  Model model;
  /** View which is controlled by this controller */
  GamePanel view;

  /**
   * Constructor to set model and view ,and to inject controller in our view
   *
   * @param model inject model in our class
   * @param view inject view in our class
   */
  public GameViewControllerImpl(Model model, GamePanel view) {
    this.model = model;
    this.view = view;
    view.setGameViewController(this);
  }
  /**
   * Method moving board to the right by his speed depends on level . Move designed by increasing X
   * while adding speed to it, if board isnt trying to go out of bounds.
   */
  @Override
  public void moveBoardToTheRight() {
    if (!model.board.isOutOfBounds()) {
      model.board.setPosX(model.board.getPosX() + model.board.getBoardSpeed());
    }
  }
  /**
   * Method moving board to the left by his speed depends on level . Move designed by decreasing X
   * while subtracting speed from it, if board isnt trying to go out of bounds.
   */
  @Override
  public void moveBoardToTheLeft() {
    if (!model.board.isOutOfBounds()) {
      model.board.setPosX(model.board.getPosX() - model.board.getBoardSpeed());
    }
  }
  /**
   * Method to move ball object on the screen by increasing or decreasing X and Y positions by ball
   * speed .
   */
  @Override
  public void moveBall() {
    checkForHit();
    checkBallForIntersects();
    if (model.blocks.isEmpty()) {
      view.changePanel(model.scoreBoard.getWinMessage(), model.scoreBoard.getScore());
      playSound(MUSIC_WIN);
    }
    if (model.ball.getY() < 0) {
      model.ball.setDirY(-model.ball.getDirY());
    }
    if (model.ball.getY() > UIConstant.FRAME_HEIGHT - 50) {
      playSound(MUSIC_LOSE);
      view.changePanel(model.scoreBoard.getLoseMessage(), model.scoreBoard.getScore());
    }
    if (model.ball.getX() < 0) {
      model.ball.setDirX(-model.ball.getDirX());
    }
    if (model.ball.getX() > view.getWidth() - model.ball.getBallLevelSize()) {
      model.ball.setDirX(-model.ball.getDirX());
    }
    model.ball.setPosX(((int) model.ball.getX()) + model.ball.getDirX());
    model.ball.setPosY(((int) model.ball.getY()) + model.ball.getDirY());
  }
  /**
   * Method check ball and board for intersection . If intersect , ballcolision method need to be
   * called.
   */
  private void checkBallForIntersects() {
    if (model.ball.getBounds().intersects(model.board.getBounds())) {
      playSound(MUSIC_JUMP);
      ballCollisions();
    }
  }
  /**
   * Method check for ball collisions , ball intersects board from the right or left corners, if its
   * not , ball leaps from board
   */
  private void ballCollisions() {
    if (model.ball.getBottomPosY() > model.board.getCornerPosY() && model.ball.getDirX() < 0) {
      view.changePanel(model.scoreBoard.getLoseMessage(), model.scoreBoard.getScore());
    } else if (model.ball.getBottomPosY() > model.board.getCornerPosY()
        && model.ball.getDirX() > 0) {
      playSound(MUSIC_LOSE);
      view.changePanel(model.scoreBoard.getLoseMessage(), model.scoreBoard.getScore());
    } else {
      model.ball.setDirY(-model.ball.getDirY());
    }
  }
  /**
   * Method check blocks for hit . If block hit and number of hits equals 0 , it need to be removed
   * from a view.
   */
  public void checkForHit() {
    Rectangle ballBounds = model.ball.getBounds();
    Block destroyed = null;
    for (Block block : model.blocks) {
      if (ballBounds.intersects(block.getBounds())) {
        playSound(MUSIC_JUMP);
        block.hit();
        if (block.isDestroyed()) {
          destroyed = block;
          playSound(MUSIC_BLOCK_CRASHED);
        }
        if (model.ball.getX() + model.ball.getBallLevelSize() - model.ball.getDirX()
            <= block.getX()) {
          model.ball.setDirX(-model.ball.getDirX());
        } else if (model.ball.getX() - model.ball.getDirX() >= block.getX() + block.getWidth()) {
          model.ball.setDirX(-model.ball.getDirX());
        } else {
          model.ball.setDirY(-model.ball.getDirY());
        }
        break;
      }
    }
    if (destroyed != null) {
      model.blocks.remove(destroyed);
      model.scoreBoard.increaseScore();
    }
  }
  /**
   * Method to providing oportunities for updating fps depends on game level
   *
   * @param timer main Timer for updating view every couple millis
   * @param timerDelay frame rate
   */
  @Override
  public void updateFps(Timer timer, int timerDelay) {
    if (timerDelay != TIMER_DELAY) {
      timerDelay = TIMER_DELAY;
      timer.setDelay(timerDelay);
    }
  }
  /**
   * Method to draw ball , board and blocks on view
   *
   * @param g graphics of panel
   */
  public void draw(Graphics g) {
    model.ball.draw(g);
    model.board.draw(g);
    model.blocks.forEach(b -> b.draw(g));
  }
}
