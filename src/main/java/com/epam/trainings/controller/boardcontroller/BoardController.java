package com.epam.trainings.controller.boardcontroller;
/**
 * BoardController Interface which provides main methods to control model of Board
 * @see com.epam.trainings.model.modelClasess.Board
 * @author epam_6th_team_lab24
 * @version 1.0
 * @since 2019-08-27
 */
public interface BoardController {
    /**
     * Method moving board to the right by his speed depends on level .
     * Move designed by increasing X while adding speed to it.
     */
    void moveBoardToTheRight();
    /**
     * Method moving board to the left by his speed depends on level .
     * Move designed by decreasing X while subtracting speed from it.
     */
    void moveBoardToTheLeft();
}
