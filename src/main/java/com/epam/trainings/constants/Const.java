package com.epam.trainings.constants;
/**
 * General constants class using for define general constants
 *
 * @author epam_6th_team_lab24
 * @version 1.0
 * @since 2019-08-27
 */
public class Const {
  public static final int NUMBER_OF_LEVELS = 3;
  /**
   * frame rate
   */
  public static int TIMER_DELAY = 5;
  public static final String FPS_HIGH = "50 fps";
  public static final String FPS_MEDIUM = "20 fps";
  public static final String FPS_LOW = "10 fps";
  public static final int FPS_HIGH_PICK = 1;
  public static final int FPS_MEDIUM_PICK = 2;
  public static final int FPS_LOW_PICK = 3;

}
