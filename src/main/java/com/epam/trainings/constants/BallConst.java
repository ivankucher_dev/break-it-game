package com.epam.trainings.constants;
/**
 * Ball constants class using for define constants of ball model
 *
 * @author epam_6th_team_lab24
 * @version 1.0
 * @since 2019-08-27
 */
public class BallConst {
  public static final int BALL_LV_1_SIZE = 45;
  public static final int BALL_LV_2_SIZE = 33;
  public static final int BALL_LV_3_SIZE = 21;
  public static final double BALL_RADIUS_FACTOR_LV_1 = 0.04;
  public static final int BALL_POS_X = 120;
  public static final int BALL_POS_Y = 350;
  /** ball speed by X position */
  public static final int BALL_DIR_X = -4;
  /** ball speed by Y position */
  public static final int BALL_DIR_Y = -6;
  public static final int BALL_DIR_X_LV_1 = -4;
  public static final int BALL_DIR_Y_LV_1 = -6;
  public static final int BALL_DIR_X_LV_2 = -6;
  public static final int BALL_DIR_Y_LV_2 = -8;
  public static final int BALL_DIR_X_LV_3 = -8;
  public static final int BALL_DIR_Y_LV_3 = -10;
}
