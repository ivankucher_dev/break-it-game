package com.epam.trainings.model;

import static com.epam.trainings.constants.UIConstant.*;
import com.epam.trainings.model.modelClasess.Ball;
import com.epam.trainings.model.modelClasess.Block;
import com.epam.trainings.model.modelClasess.Board;
import com.epam.trainings.model.modelClasess.ScoreBoard;
import static com.epam.trainings.constants.BallConst.*;
import static com.epam.trainings.constants.BoardConst.*;
import static com.epam.trainings.constants.BlockConst.*;
import java.util.ArrayList;
import java.util.Random;

/**
 * class that contains data needed to play the game
 * @author epam_6th_team_lab24
 */
public class Model {

  public Ball ball;
  public Board board;
  public ArrayList<Block> blocks;
  public ScoreBoard scoreBoard;

  public Model() {
    int ballPosX = randomBallSpawn();
    ball = new Ball(ballPosX, BALL_POS_Y, BALL_RADIUS_FACTOR_LV_1);
    board = new Board(BOARD_POS_X, 600, 400, 8);
    blocks = new ArrayList<>();
    scoreBoard = new ScoreBoard();
  }

  private int randomBallSpawn() {
    return BALL_POS_X + (int) (Math.random() * 200 - 60);
  }

  public void initializeBlocks(final int width, final int height) {
    int startPositionX = ((FRAME_WIDTH - (width * NUMBER_OF_BLOCKS_ACROSS)) / 2) - 30;
    int positionX = startPositionX;
    int positionY = START_POS_Y_FOR_BLOCK_INIT;
    Random random = new Random();
    int hits;
    for (int i = 0; i < NUMBER_OF_BLOCKS_IN_HEIGHT; i++) {
      for (int j = 0; j < NUMBER_OF_BLOCKS_ACROSS; j++) {
        hits = random.nextInt(MAX_NUMBER_OF_HITS_TO_DESTROY_BLOCK);
        this.blocks.add(new Block(positionX, positionY, height, width, hits));
        positionX += width + DISTANCE_BEETWEEN_BLOCKS;
      }
      positionY += height + width / DISTANCE_BEETWEEN_BLOCKS;
      positionX = startPositionX;
    }
  }

  /**
   * method that resets all params if you lose or win
   * and want to play again
   */
  public void recreateGameForNew() {
    blocks = new ArrayList<>();
    ball.setPosX(randomBallSpawn());
    ball.setPosY(BALL_POS_Y);
    board.setPosX(BOARD_POS_X);
    scoreBoard = new ScoreBoard();
  }
}
