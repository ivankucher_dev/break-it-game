package com.epam.trainings.view;

import com.epam.trainings.constants.UIConstant;
import com.epam.trainings.controller.viewcontroller.gameviewcontroller.GameViewControllerImpl;
import com.epam.trainings.utils.PanelWorker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import static com.epam.trainings.constants.Const.*;

/**
 * Panel for showing game content
 *
 * @author epam_6th_team_lab24
 */
public class GamePanel extends JPanel implements KeyListener, ActionListener {

  private GameViewControllerImpl controller;
  private Timer timer;
  private int timerDelay;
  private GameOverPanel gameOverPanel;

  public GamePanel(GameOverPanel gameOverPanel) {
    this.setOpaque(false);
    setFocusable(true);
    setFocusTraversalKeysEnabled(false);
    addKeyListener(this);
    this.gameOverPanel = gameOverPanel;
    timerDelay = TIMER_DELAY;
    timer = new Timer(timerDelay, this);
    timer.start();
    this.setPreferredSize(new Dimension(UIConstant.FRAME_WIDTH, UIConstant.FRAME_WIDTH));
    this.setMinimumSize(new Dimension(UIConstant.FRAME_WIDTH, UIConstant.FRAME_WIDTH));
  }

  public void setGameViewController(GameViewControllerImpl viewController) {
    this.controller = viewController;
  }

  /**
   * method for drawing game components(ball, blocks, board)
   *
   * @param g graphics object
   */
  public void paint(Graphics g) {
    controller.draw(g);
    controller.moveBall();
  }

  public void updateTimerDelay() {
    controller.updateFps(timer, timerDelay);
  }

  /**
   * event handler for timer event
   *
   * @param e event object
   */
  public void actionPerformed(ActionEvent e) {
    if (e.getSource() == timer) {
      this.repaint();
    }
  }

  public void keyTyped(KeyEvent e) {}

  /**
   * method for handling user input
   *
   * @param e event object
   */
  public void keyPressed(KeyEvent e) {
    if (e.getKeyCode() == KeyEvent.VK_LEFT) {
      controller.moveBoardToTheLeft();
    }
    if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
      controller.moveBoardToTheRight();
    }
  }

  public void keyReleased(KeyEvent e) {}

  /**
   * change panel after game is finished
   *
   * @param message message for user(win or lose)
   * @param score number of blocks, that user destroyed
   */
  public void changePanel(String message, int score) {
    this.gameOverPanel.setMessage(message);
    this.gameOverPanel.setScore(score);
    this.gameOverPanel.init();
    JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
    PanelWorker.changePanel(gameOverPanel, topFrame);
  }
}
