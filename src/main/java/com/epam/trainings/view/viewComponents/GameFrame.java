package com.epam.trainings.view.viewComponents;

import com.epam.trainings.constants.UIConstant;
import com.epam.trainings.utils.FileWorker;
import com.epam.trainings.view.MenuPanel;
import static com.epam.trainings.constants.UIConstant.*;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.io.IOException;
/**
 * This is main frame of program,
 * in which we displaying different panels
 * @author epam_6th_team_lab24
 */
public class GameFrame extends JFrame {


  MenuPanel menuPanel;
  BackgroundPanel backgroundPanel;

  public GameFrame() {
    super("Break-it");
    createGUI();
    try {
      setIconImage(ImageIO.read(FileWorker.getFileFromURL(APP_ICON,this)));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Setting panel which, are used in program
   * @param menuPanel panel for displaying start menu
   * @param backgroundPanel panel with background image
   */
  public void init(MenuPanel menuPanel, BackgroundPanel backgroundPanel) {
    this.menuPanel = menuPanel;
    this.backgroundPanel = backgroundPanel;
    this.setContentPane(this.backgroundPanel);
    this.getContentPane().add(menuPanel);
    setVisible(true);
  }

  /**
   * setting up values for frame
   */
  public void createGUI() {
    setSize(UIConstant.FRAME_WIDTH, UIConstant.FRAME_HEIGHT);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setResizable(false);
  }
}
