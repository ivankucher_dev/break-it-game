package com.epam.trainings.view.viewComponents;

import com.epam.trainings.constants.UIConstant;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

/**
 * This panel is used for displaying background image in all views
 * @author epam_6th_team_lab24
 */
public class BackgroundPanel extends JPanel {
  public BackgroundPanel() {
    this.setLayout(new GridBagLayout());
  }

  /**
   * Overriding paintComponent method for drawing background image
   * @param g graphics on which we draw background image
   */
  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    try {
      BufferedImage bi =
          ImageIO.read(new File(UIConstant.BACKGROUND_IMAGE_PATH).toURI().toURL());
      g.drawImage(bi, 0, 0, getWidth(), getHeight(), this);
    } catch (MalformedURLException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
