package com.epam.trainings.view;

import com.epam.trainings.controller.viewcontroller.settingsviewcontroller.SettingsViewControllerImpl;
import com.epam.trainings.utils.SetupRadioButtons;
import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import static com.epam.trainings.utils.PanelWorker.*;
import static com.epam.trainings.constants.UIConstant.*;
import static com.epam.trainings.constants.Const.*;

/**
 * Settings panel for selecting fps
 * @author epam_6th_team_lab24
 */
public class SettingsPanel extends JPanel {
  private JButton setFpsButton;
  private MenuPanel menuPanel;
  private List<JRadioButton> levelButtons;
  private ButtonGroup buttonGroup;
  private SettingsViewControllerImpl viewController;
  ArrayList<String> fps = new ArrayList<>();

  public SettingsPanel(MenuPanel menuPanel) {
    this.levelButtons = new ArrayList<>();
    fps.add(FPS_HIGH);
    fps.add(FPS_MEDIUM);
    fps.add(FPS_LOW);
    initUI();
    this.menuPanel = menuPanel;
  }

  public void setViewController(SettingsViewControllerImpl viewController) {
    this.viewController = viewController;
  }

  /**
   * initializing of UI, adding control buttons
   */
  private void initUI() {
    this.setOpaque(false);
    GroupLayout layout = new GroupLayout(this);
    this.setLayout(layout);
    setFpsButton = new JButton("Set");
    setFpsButton.addActionListener(
        a -> {
          viewController.changeFps(Integer.valueOf(buttonGroup.getSelection().getActionCommand()));
          JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
          changePanel(menuPanel, topFrame);
        });
    this.buttonGroup = new ButtonGroup();
    for (int i = 0; i < fps.size(); i++) {
      JRadioButton levelButton = this.createFpsButton(fps.get(i));
      this.levelButtons.add(levelButton);
      buttonGroup.add(levelButton);
    }
    layout.setAutoCreateGaps(true);
    layout.setAutoCreateContainerGaps(true);
    GroupLayout.Group verticalGroup = layout.createSequentialGroup();
    GroupLayout.Group horizontalGroup = layout.createParallelGroup();
    SetupRadioButtons.setupRadioButtonsOnView(verticalGroup,horizontalGroup,levelButtons);
    verticalGroup.addComponent(setFpsButton);
    horizontalGroup.addComponent(setFpsButton);
    layout.setVerticalGroup(verticalGroup);
    layout.setHorizontalGroup(horizontalGroup);
  }

  /**
   * creating radio button for selecting fps
   * @return JRadioButton
   */
  private JRadioButton createFpsButton(String fps) {
    int levelNumber = levelButtons.size() + START_LEVEL_VALUE;
    JRadioButton levelButton = new JRadioButton(fps);
    levelButton.setActionCommand(Integer.toString(levelNumber));
    levelButton.setSelected(true);
    levelButton.setOpaque(false);
    levelButton.setForeground(new Color(255, 255, 255, 255));
    return levelButton;
  }
}
