package com.epam.trainings.view;

import com.epam.trainings.controller.viewcontroller.gameovercontroller.GameOverController;
import static com.epam.trainings.utils.PanelWorker.changePanel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.*;

/**
 * panel for displaying results of game
 *
 * @author epam_6th_team_lab24
 */
public class GameOverPanel extends JPanel {
  private static final String MESSAGE_TITLE = "Message: ";
  private static final String SCORE_TITLE = "Score: ";
  private static final String QUIT_BUTTON_TITLE = "Quit";
  private static final String MENU_BUTTON_TITLE = "Menu";
  private String message;
  private int score;
  private Map<JLabel, JLabel> titleToValue;
  private List<JButton> controls;
  private GroupLayout layout;
  private MenuPanel menuPanel;
  private GameOverController gameOverController;

  public void setGameOverController(GameOverController gameOverController) {
    this.gameOverController = gameOverController;
  }

  /** this method is used for initializing our panel, before showing to user */
  public void init() {
    initTitleToValue();
    initControls();
    createLayout();
  }

  /** this method is used for creating layout for panel and adding components to it */
  private void createLayout() {
    this.removeAll();
    this.layout = new GroupLayout(this);
    this.setLayout(layout);
    layout.setAutoCreateGaps(true);
    layout.setAutoCreateContainerGaps(true);
    GroupLayout.Group verticalGroup = layout.createSequentialGroup();
    GroupLayout.Group horizontalGroup = layout.createParallelGroup();
    titleToValue.forEach(
        (k, v) -> {
          verticalGroup.addGroup(layout.createParallelGroup().addComponent(k).addComponent(v));
          horizontalGroup.addGroup(layout.createSequentialGroup().addComponent(k).addComponent(v));
        });
    GroupLayout.Group controlsVerticalGroup = layout.createParallelGroup();
    GroupLayout.Group controlsHorizontalGroup = layout.createSequentialGroup();
    controls.forEach(
        c -> {
          controlsHorizontalGroup.addComponent(c);
          controlsVerticalGroup.addComponent(c);
        });
    verticalGroup.addGroup(controlsVerticalGroup);
    horizontalGroup.addGroup(controlsHorizontalGroup);
    layout.setVerticalGroup(verticalGroup);
    layout.setHorizontalGroup(horizontalGroup);
  }

  /** this method initialize titleToValue map, which contains values for showing results of game */
  private void initTitleToValue() {
    this.titleToValue = new HashMap<>();
    JLabel messageTitleLabel = new JLabel(MESSAGE_TITLE);
    JLabel messageValueLabel = new JLabel(message);
    JLabel scoreTitleLabel = new JLabel(SCORE_TITLE);
    JLabel scoreValueLabel = new JLabel(Integer.toString(score));
    titleToValue.put(messageTitleLabel, messageValueLabel);
    titleToValue.put(scoreTitleLabel, scoreValueLabel);
  }

  /** this method is used for initialize control buttons (go to menu, quit) */
  private void initControls() {
    this.controls = new ArrayList<>();
    JButton menuButton = new JButton(MENU_BUTTON_TITLE);
    JButton quitButton = new JButton(QUIT_BUTTON_TITLE);
    menuButton.addActionListener(
        a -> {
          JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
          gameOverController.recreateGame();
          changePanel(menuPanel, topFrame);
        });
    quitButton.addActionListener(
        a -> {
          System.exit(0);
        });
    this.controls.add(menuButton);
    this.controls.add(quitButton);
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public void setScore(int score) {
    this.score = score;
  }

  public void setMenuPanel(MenuPanel menuPanel) {
    this.menuPanel = menuPanel;
  }
}
