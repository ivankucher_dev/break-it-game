package com.epam.trainings.view;

import com.epam.trainings.constants.Const;
import com.epam.trainings.controller.viewcontroller.startmenucontroller.ViewController;
import com.epam.trainings.utils.SetupRadioButtons;
import com.epam.trainings.utils.SoundsWorker;
import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.GroupLayout.Alignment;
import static com.epam.trainings.utils.PanelWorker.*;
import static com.epam.trainings.constants.UIConstant.*;

/**
 * Panel for showing start menu (start game, settings, quit)
 *
 * @author epam_6th_team_lab24
 */
public class MenuPanel extends JPanel {
  private JButton startButton;
  private JButton settingsButton;
  private JButton soundButton;
  private JButton quitButton;
  private GamePanel gamePanel;
  private SettingsPanel settingsPanel;
  private List<JRadioButton> levelButtons;
  private ButtonGroup buttonGroup;
  private ViewController viewController;
  private JLabel title;

  public MenuPanel(GamePanel gamePanel) {
    this.levelButtons = new ArrayList<>();
    this.title = initTitle();
    initUI();
    this.gamePanel = gamePanel;
  }

  private JLabel initTitle() {
    JLabel title = new JLabel(GAME_TITLE);
    Font titleFont = new Font("TimesRoman", Font.BOLD, 72);
    title.setFont(titleFont);
    title.setForeground(Color.white);
    return title;
  }

  public void setSettingsPanel(SettingsPanel settingsPanel) {
    this.settingsPanel = settingsPanel;
  }

  public void setViewController(ViewController viewController) {
    this.viewController = viewController;
  }

  /** initializing of UI, adding layout, control buttons */
  private void initUI() {
    this.setOpaque(false);
    GroupLayout layout = new GroupLayout(this);
    this.setLayout(layout);
    initButtons();
    initGroupLayout(layout);
    SoundsWorker.playSound(MUSIC_BACKGROUND);
  }

  private void initGroupLayout(GroupLayout layout) {
    layout.setAutoCreateGaps(true);
    layout.setAutoCreateContainerGaps(true);
    GroupLayout.Group verticalGroup = layout.createSequentialGroup();
    GroupLayout.Group horizontalGroup = layout.createParallelGroup(Alignment.CENTER);
    verticalGroup.addComponent(this.title);
    verticalGroup.addGap(TITLE_GAP_SIZE);
    horizontalGroup.addComponent(this.title);
    SetupRadioButtons.setupRadioButtonsOnView(verticalGroup, horizontalGroup, levelButtons);
    verticalGroup
        .addComponent(startButton)
        .addComponent(settingsButton)
        .addComponent(soundButton)
        .addComponent(quitButton);
    horizontalGroup
        .addComponent(startButton)
        .addComponent(settingsButton)
        .addComponent(soundButton)
        .addComponent(quitButton);
    layout.setVerticalGroup(verticalGroup);
    layout.setHorizontalGroup(horizontalGroup);
    layout.linkSize(startButton, settingsButton, soundButton, quitButton);
  }

  private void initButtons() {
    startButton = new JButton(START_BUTTON_TITLE);
    settingsButton = new JButton(SETTINGS_BUTTON_TITLE);
    quitButton = new JButton(QUIT_BUTTON_TITLE);
    ImageIcon imageForOne = new ImageIcon(getClass().getClassLoader().getResource(SOUND_BUTTON));
    soundButton = new JButton("", imageForOne);
    soundButton.setPreferredSize(new Dimension(28, 28));
    startButton.addActionListener(
        a -> {
          viewController.levelPicked(
              Integer.valueOf(buttonGroup.getSelection().getActionCommand()));
          JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
          gamePanel.updateTimerDelay();
          viewController.changePanel(gamePanel, topFrame);
        });

    settingsButton.addActionListener(
        a -> {
          JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
          changePanel(settingsPanel, topFrame);
        });
    soundButton.addActionListener(
        a -> {
          viewController.muteSounds();
        });
    quitButton.addActionListener(
        a -> {
          System.exit(0);
        });
    this.buttonGroup = new ButtonGroup();
    for (int i = 0; i < Const.NUMBER_OF_LEVELS; i++) {
      JRadioButton levelButton = this.createLevelButton();
      this.levelButtons.add(levelButton);
      buttonGroup.add(levelButton);
    }
  }

  /**
   * creating radio button for selecting level
   *
   * @return JRadioButton
   */
  private JRadioButton createLevelButton() {
    int levelNumber = levelButtons.size() + START_LEVEL_VALUE;
    JRadioButton levelButton = new JRadioButton(LEVEL_TITLE + levelNumber);
    levelButton.setActionCommand(Integer.toString(levelNumber));
    levelButton.setSelected(true);
    levelButton.setOpaque(false);
    levelButton.setForeground(new Color(255, 255, 255, 255));
    return levelButton;
  }
}
